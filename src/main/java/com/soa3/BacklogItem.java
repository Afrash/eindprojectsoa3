package com.soa3;

import com.soa3.BacklogItemStates.*;

import java.util.ArrayList;
import java.util.List;

public class BacklogItem extends NotificationObservable {

    private String title;
    private Person person;
    private List<Activity> activities;
    private Backlog backlog;
    private boolean isDone;
    private boolean testedIsSuccesfull;


    IBacklogState backlogState;

    IBacklogState toDoState;
    IBacklogState doingState;
    IBacklogState readyForTestingState;
    IBacklogState testingState;
    IBacklogState doneAndTestedState;
    IBacklogState doneState;

    private Sprint sprint;

    public BacklogItem(String title) {
        this.title = title;
        this.activities = new ArrayList<Activity>();
        this.isDone = false;
        this.testedIsSuccesfull = false;



        this.toDoState = new ToDoState(this);
        this.doingState = new DoingState(this);
        this.readyForTestingState = new ReadyForTestingState(this);
        this.testingState = new TestingState(this);
        this.doneAndTestedState = new DoneAndTestedState(this);
        this.doneState = new DoneState(this);

        this.backlogState = toDoState;
    }


    public void toDo(){
        this.backlogState.toDo();
    }

    public void doing(){
        this.backlogState.doing();
    }

    public void readyForTesting(){
        this.backlogState.readyForTesting();
    }

    public void testing(){
        this.backlogState.testing();
    }

    public void doneAndTesting(){
        this.backlogState.doneAndTesting();
    }

    public void done(){
        this.backlogState.doneState();
    }

    public IBacklogState getDoneState() {
        return doneState;
    }

    public IBacklogState getBacklogState() {
        return backlogState;
    }

    public void setBacklogState(IBacklogState backlogState) {
        this.backlogState = backlogState;
    }

    public IBacklogState getToDoState() {
        return toDoState;
    }

    public IBacklogState getDoingState() {
        return doingState;
    }

    public IBacklogState getReadyForTestingState() {
        return readyForTestingState;
    }

    public IBacklogState getTestingState() {
        return testingState;
    }

    public IBacklogState getDoneAndTestedState() {
        return doneAndTestedState;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }


    public boolean isTestedIsSuccesfull() {
        return testedIsSuccesfull;
    }

    public void setTestedIsSuccesfull(boolean testedIsSuccesfull) {
        this.testedIsSuccesfull = testedIsSuccesfull;
    }

    public void addActivity(Activity activity){
        activities.add(activity);
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        if(done){
            isDone = checkIfItemIsDone();
        }else{
            isDone = false;
        }
    }

    public boolean checkIfItemIsDone(){
        boolean check = true;
        if(this.activities.size() > 1){
            for (Activity activity : this.activities) {
                if (!activity.isDone()) {
                    check = false;
                    break;
                }
            }
        }
        else{
            return true;
        }
        return check;
    }

    public void removeActivity(Activity activity){
        activities.remove(activity);
    }

    public Backlog getBacklog() {
        return backlog;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }


}

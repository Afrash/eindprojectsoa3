package com.soa3;

public interface NotificationObserver {
    void update(String message);
}

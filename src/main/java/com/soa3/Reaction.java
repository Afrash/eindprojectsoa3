package com.soa3;

import java.util.ArrayList;
import java.util.List;

public class Reaction {
    private String message;
    private Person person;

    private List<Reaction> replies;

    public Reaction(String message, Person person) {
        this.message = message;
        this.person = person;
        this.replies = new ArrayList<Reaction>();
    }

    public void addReply(Reaction reply){
        replies.add(reply);
    }

    public List<Reaction> getReplies() {
        return replies;
    }

}

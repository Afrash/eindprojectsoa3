package com.soa3;

public class SlackObserver implements NotificationObserver {

    private NotificationObservable observable;
    private String message;

    public SlackObserver(NotificationObservable observable) {
        this.observable = observable;
        this.observable.addObserver(this);
    }

    @Override
    public void update(String message) {
        this.message = message;
        display();
    }

    private void display(){
        System.out.println(message + " send with Slack");
    }

    public NotificationObservable getObservable() {
        return observable;
    }

    public String getMessage() {
        return message;
    }
}

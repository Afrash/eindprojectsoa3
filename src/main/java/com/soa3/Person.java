package com.soa3;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String type;
    private String name;

    public Person(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}

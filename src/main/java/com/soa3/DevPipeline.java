package com.soa3;

public class DevPipeline implements IDevops{
    private Sprint sprint;
    private boolean isRunning;

    public DevPipeline(Sprint sprint) {
        this.sprint = sprint;
        this.isRunning = true;
    }
    @Override
    public void createSource() {
        System.out.println("Create source");
    }

    @Override
    public void createPackages() {
        System.out.println("Create packages");
    }

    @Override
    public void createBuild() {
        System.out.println("Create build");
    }

    @Override
    public void createTests() {
        System.out.println("Create tests");
        isRunning = false;
    }

    @Override
    public void createAnalysis() {

    }

    @Override
    public void createDeploy() {

    }

    @Override
    public void createUtility() {

    }
}

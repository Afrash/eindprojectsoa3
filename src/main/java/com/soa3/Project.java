package com.soa3;

import java.util.List;

public class Project {

    private String name;
    private int id;
    private Backlog backlog;
    private List<Person> persons;

    public Project(String name, int id, List<Person> persons) {
        this.name = name;
        this.id = id;
        this.backlog = new Backlog(this);
        this.persons = persons;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Backlog getBacklog() {
        return backlog;
    }

}

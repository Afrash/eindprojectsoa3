package com.soa3;

import java.util.ArrayList;
import java.util.List;

public class Backlog {
    private List<BacklogItem> backlogItems;
    private Project project;

    public Backlog(Project project) {
        this.backlogItems = new ArrayList<BacklogItem>();
        this.project = project;
    }

    public List<BacklogItem> getBacklogItems() {
        return backlogItems;
    }

    public void addBacklogItem(BacklogItem backlogItem){
        backlogItems.add(backlogItem);
        backlogItem.setBacklog(this);
    }
}

package com.soa3;

public class Activity {
    private Person person;
    private String title;
    private boolean isDone;

    public Activity(Person person, String title) {
        this.person = person;
        this.title = title;
        this.isDone = false;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}

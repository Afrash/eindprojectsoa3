package com.soa3;

public class SMS {

    private String message;
    private String phoneNumber;

    public void getPhoneNumber(String number){
        this.phoneNumber=number;
    }

    public void getMessage(String message){
        this.message=message;
    }

    public void sent(){
        System.out.println("connect to phone network");
        System.out.println("message sent: "+ this.message+" to: "+this.phoneNumber);
    }
}

package com.soa3;

import java.util.ArrayList;
import java.util.List;

public class ForumThread {

    private BacklogItem backlogItem;

    private List<Reaction> reactions;

    private Person person;
    public ForumThread(BacklogItem backlogItem, Person person) {
        this.backlogItem = backlogItem;
        this.reactions = new ArrayList<Reaction>();
        this.person=person;
    }

    public List<Reaction> getReactions() {
        return reactions;
    }

    public void addReaction(Reaction reaction){
        reactions.add(reaction);
    }
}

package com.soa3;

import com.soa3.ExportBehavior.SprintExportBehavior;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Sprint {

    private String name;
    private LocalDate startDate;
    private LocalDate endDate;
    private String type;
    private List<Person> developers;
    private Person scrumMaster;
    private Person po;
    private boolean isActive;
    private boolean isFinished;
    private List<BacklogItem> backlogItems;
    private List<Person> persons;

    public SprintExportBehavior sprintExportBehavior;

    public Sprint(String name, LocalDate startDate, LocalDate endDate, String type, List<Person> developers, Person scrumMaster, Person po) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.isFinished = false;
        this.backlogItems = new ArrayList<BacklogItem>();
        this.persons = new ArrayList<Person>();
        this.isActive=false;
        this.developers = developers;
        this.scrumMaster = scrumMaster;
        this.po = po;
    }

    public void runPipeline(){
        IDevops devops;
        if(this.type.equals("release")){
            devops = new ReleaseDevPipeline(this);
            devops.createSource();
            devops.createPackages();
            devops.createBuild();
            devops.createTests();
            devops.createAnalysis();
            devops.createUtility();
            devops.createDeploy();
        }else{
            devops = new DevPipeline(this);
            devops.createSource();
            devops.createPackages();
            devops.createBuild();
            devops.createTests();
            devops.createAnalysis();
        }
    }

    public void addBacklogItem(BacklogItem backlogItem){
        backlogItems.add(backlogItem);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BacklogItem> getBacklogItems() {
        return backlogItems;
    }

    public void export(SprintExportBehavior sprintExportBehavior) throws Exception {
        sprintExportBehavior.export(this);
    }
}

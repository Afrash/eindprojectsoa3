package com.soa3;

public class SMSObserverAdapter implements NotificationObserver{
    private NotificationObservable observable;
    private SMS sms;

    public SMSObserverAdapter(NotificationObservable observable) {
        this.observable = observable;
        this.sms = new SMS();
        this.observable.addObserver(this);
    }

    public NotificationObservable getObservable() {
        return observable;
    }

    @Override
    public void update(String message) {
        sms.getMessage(message);

        //method to get phonenumber
        sms.getPhoneNumber("06329423894");
        display();
    }

    private void display(){
        sms.sent();
    }
}

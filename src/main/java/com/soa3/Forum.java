package com.soa3;

import java.util.ArrayList;
import java.util.List;

public class Forum {

    private List<ForumThread> forumThreads;


    public Forum() {
        this.forumThreads = new ArrayList<ForumThread>();
    }

    public void addForumThread(ForumThread forumThread){
        forumThreads.add(forumThread);
    }

    public List<ForumThread> getForumThreads() {
        return forumThreads;
    }

}

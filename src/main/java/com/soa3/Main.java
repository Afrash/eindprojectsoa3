package com.soa3;

import com.soa3.ExportBehavior.SprintExportToPdfBehavior;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
//        Forum forum = new Forum();
//        List<Person> persons = new ArrayList<>();
//        List<Person> developers = new ArrayList<>();
//        Person p1 = new Person("Tester","Spoderman");
//        Person p2 = new Person("Developer", "Leonardo");
//        Person p3 = new Person("Scrummaster", "Matt Murdock");
//        Person p4 = new Person("Productowner", "PO");
//        Person p5 = new Person("Developer", "Dev");
//        developers.add(p2);
//        developers.add(p5);
//        persons.add(p1);
//        persons.add(p2);
//        persons.add(p3);
//        persons.add(p4);
//
//        Project project = new Project("projectnaam",1,persons);
//
//        Backlog backlog = project.getBacklog();
//
//        backlog.addBacklogItem(new BacklogItem("backend"));
//        backlog.addBacklogItem(new BacklogItem("frontend"));
//        backlog.addBacklogItem(new BacklogItem("database"));
//
//        new SMSObserverAdapter(backlog.getBacklogItems().get(0));
//        backlog.getBacklogItems().get(0).notifyObservers("test");
//
//        backlog.getBacklogItems().get(0).setPerson(p3);
//        backlog.getBacklogItems().get(1).setPerson(p1);
//        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
//        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));
//
//        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
//        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
//        sprint.getBacklogItems().get(0).setSprint(sprint);
//        sprint.addBacklogItem(backlog.getBacklogItems().get(1));
//        sprint.getBacklogItems().get(1).setSprint(sprint);
//        sprint.addBacklogItem(backlog.getBacklogItems().get(2));
//        sprint.getBacklogItems().get(2).setSprint(sprint);
//
//        System.out.println("TESGING");
//        sprint.getBacklogItems().get(0).getBacklogState().toDo();
//        sprint.getBacklogItems().get(0).getBacklogState().doing();
//        System.out.println(sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
//
//
//        sprint.export(new SprintExportToPdfBehavior());

//        System.out.println(sprint.getBacklogItems().get(2).isDone());
//        sprint.getBacklogItems().get(2).getActivities().get(0).setDone(true);
//        sprint.getBacklogItems().get(2).getActivities().get(1).setDone(true);
//        sprint.getBacklogItems().get(2).setDone(true);
//        System.out.println(sprint.getBacklogItems().get(2).isDone());

//        forum.addForumThread(new com.soa3.ForumThread(sprint.getBacklogItems().get(0),p1));
//        com.soa3.ForumThread ft = forum.getForumThreads().get(0);
//        ft.addReaction(new com.soa3.Reaction("reaction",p2));
//        ft.addReaction(new com.soa3.Reaction("reaction2",p3));
//        ft.getReactions().get(0).addReply(new com.soa3.Reaction("reply",p1));
//
//        sprint.setFinished(true);
//        boolean positive = true;
//        for (com.soa3.BacklogItem b: sprint.getBacklogItems()) {
//            if(!b.isDone()){
//                positive = false;
//                break;
//            }
//        }
//
//        if(positive){
//            if(Objects.equals(sprint.getType(), "release")){
//                sprint.runPipeline();
//                for (com.soa3.Person person : persons) {
//                    if (person.getType().equals("Scrummaster")|| person.getType().equals("productowner")) {
//                        System.out.println("sprint released");
//                    }
//                }
//            }
//            sprint.setActive(false);
//        }else{
//            if(Objects.equals(sprint.getType(), "release")){
//                for (com.soa3.Person person : persons) {
//                    if (person.getType().equals("Scrummaster")|| person.getType().equals("productowner")) {
//                        System.out.println("cancelled notification");
//                    }
//                }
//            }
//            sprint.setActive(false);
//        }

//        ISCM scm = new com.soa3.SCM();
//
//        Commit commitCmd = new Commit(scm,"commit message");
//
//        CMD commit = new CMD(commitCmd);
//
//        commit.runCmd();
//
//        Push pushCmd = new Push(scm);
//
//        CMD push = new CMD(pushCmd);
//
//        push.runCmd();
        //sprint.export(new com.soa3.ExportBehavior.SprintExportToPdfBehavior());
//        NotificationObservable notificationObservable = new com.soa3.NotificationObservable();
//        SMSObserverAdapter smsObserverAdapter = new com.soa3.SMSObserverAdapter(notificationObservable);
//        notificationObservable.addObserver(smsObserverAdapter);
//        smsObserverAdapter.update("com.soa3.Sprint is done");
    }

}

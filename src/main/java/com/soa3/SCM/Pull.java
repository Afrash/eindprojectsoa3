package com.soa3.SCM;

public class Pull implements Command{

    ISCM scm;

    public Pull(ISCM scm) {
        this.scm = scm;
    }

    @Override
    public void execute() {
        scm.pull();
    }
}

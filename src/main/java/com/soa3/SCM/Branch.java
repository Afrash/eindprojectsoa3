package com.soa3.SCM;

public class Branch implements Command{
    ISCM scm;
    String name;

    public Branch(ISCM scm,String name) {
        this.scm = scm;
        this.name = name;
    }

    @Override
    public void execute() {
        scm.branch(this.name);
    }
}

package com.soa3.SCM;

public class Commit implements Command{
    ISCM scm;
    String message;

    public Commit(ISCM scm, String message) {
        this.scm = scm;
        this.message = message;
    }

    @Override
    public void execute() {
        scm.commit(this.message);
    }
}

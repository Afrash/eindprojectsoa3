package com.soa3.SCM;

public class Push implements Command{

    ISCM scm;

    public Push(ISCM scm) {
        this.scm = scm;
    }

    @Override
    public void execute() {
        scm.push();
    }
}

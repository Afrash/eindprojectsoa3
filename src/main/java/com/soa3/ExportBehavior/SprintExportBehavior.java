package com.soa3.ExportBehavior;

import com.soa3.Sprint;

public interface SprintExportBehavior {

    void export(Sprint sprint) throws Exception;
}

package com.soa3.ExportBehavior;

import com.soa3.Sprint;

import java.io.FileWriter;

public class SprintExportToPngBehavior implements SprintExportBehavior {
    @Override
    public void export(Sprint sprint) throws Exception {
        System.out.println("Printed sprint in png format");
//        FileWriter writer = new FileWriter("testfile.png");
//        writer.write(sprint.getName());
//        writer.close();
    }
}

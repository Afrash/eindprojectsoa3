package com.soa3.ExportBehavior;

import com.soa3.Sprint;

import java.io.FileWriter;

public class SprintExportToPdfBehavior implements SprintExportBehavior {
    @Override
    public void export(Sprint sprint) throws Exception {
        System.out.println("Report in pdf");
//        FileWriter writer = new FileWriter("testfile.pdf");
//        writer.write(sprint.getName());
//        writer.close();
    }
}

package com.soa3;

public class ReleaseDevPipeline implements IDevops{
    private Sprint sprint;
    private boolean isRunning;

    public ReleaseDevPipeline(Sprint sprint) {
        this.sprint = sprint;
        this.isRunning = true;
    }

    @Override
    public void createSource() {
        System.out.println("Create source");
    }

    @Override
    public void createPackages() {
        System.out.println("Create packages");
    }

    @Override
    public void createBuild() {
        System.out.println("Create build");
    }

    @Override
    public void createTests() {
        System.out.println("Create tests");
    }

    @Override
    public void createAnalysis() {
        System.out.println("Create analysis");
    }

    @Override
    public void createDeploy() {
        System.out.println("Create deploy");
        isRunning = false;
    }

    @Override
    public void createUtility() {
        System.out.println("Create utility");
    }
}

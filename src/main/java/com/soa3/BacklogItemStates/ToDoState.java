package com.soa3.BacklogItemStates;

import com.soa3.BacklogItem;

public class ToDoState implements IBacklogState {

    private BacklogItem backlogItem;


    public ToDoState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void toDo() {
        System.out.println("ToDoState: Backlog item is selected by person");
        backlogItem.setBacklogState(backlogItem.getDoingState());
    }

    @Override
    public void doing() {
        System.out.println("ToDoState: Create a backlog item first");
    }

    @Override
    public void readyForTesting() {
        System.out.println("ToDoState: Create a backlog item first");
    }

    @Override
    public void testing() {
        System.out.println("ToDoState: Create a backlog item first");
    }

    @Override
    public void doneAndTesting() {
        System.out.println("ToDoState: Create tests");
    }

    @Override
    public void doneState() {
        System.out.println("State is not here");
    }
}

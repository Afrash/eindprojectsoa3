package com.soa3.BacklogItemStates;

import com.soa3.BacklogItem;

public class DoneState implements IBacklogState {

    private BacklogItem backlogItem;

    public DoneState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void toDo() {
        System.out.println("Done state: no to do");
    }

    @Override
    public void doing() {
        System.out.println("Done state:no doing");
    }

    @Override
    public void readyForTesting() {
        System.out.println("Done state:no testing yet");
    }

    @Override
    public void testing() {
        System.out.println("Done state:no tests");
    }

    @Override
    public void doneAndTesting() {
        System.out.println("Done state:not done for testing");
    }

    @Override
    public void doneState() {
        System.out.println("Done state:Done state");
    }
}

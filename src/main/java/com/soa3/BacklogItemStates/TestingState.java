package com.soa3.BacklogItemStates;

import com.soa3.Activity;
import com.soa3.BacklogItem;
import com.soa3.EmailObserver;

public class TestingState implements IBacklogState {

    private BacklogItem backlogItem;

    public TestingState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void toDo() {
        System.out.println("testing state: Order selected by developer");
    }

    @Override
    public void doing() {
        System.out.println("testing state:Can't go back to doing");
    }

    @Override
    public void readyForTesting() {
        System.out.println("Backlog item is ready ");
    }

    @Override
    public void testing() {
        if (this.backlogItem.getPerson().getType().equals("Scrummaster")) {
            if(backlogItem.getPerson()==null){
                for (Activity a: backlogItem.getActivities()) {
                    new EmailObserver(this.backlogItem);
                    this.backlogItem.notifyObservers("Scrummaster notification "+a.getPerson().getName());
                }
            }else{
                new EmailObserver(this.backlogItem);
                this.backlogItem.notifyObservers("Scrummaster notification "+backlogItem.getPerson().getName());
            }
        }

        if(!this.backlogItem.isTestedIsSuccesfull()){
            backlogItem.setBacklogState(new ToDoState(backlogItem));
        }

        if (this.backlogItem.getPerson().getType().equals("Tester")) {
            System.out.println("testing");
            backlogItem.setBacklogState(backlogItem.getDoneAndTestedState());
        }
    }

    @Override
    public void doneAndTesting() {
        System.out.println("testing state: It is not tested");
    }

    @Override
    public void doneState() {
        System.out.println("testing state:Not done");
    }
}

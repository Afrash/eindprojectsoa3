package com.soa3.BacklogItemStates;

import com.soa3.BacklogItem;

public class DoneAndTestedState implements IBacklogState {

    private BacklogItem backlogItem;

    public DoneAndTestedState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void toDo() {
        System.out.println("Done and tested: Order is selected by developer");
    }

    @Override
    public void doing() {
        System.out.println("Done and tested: no doing yet");
    }

    @Override
    public void readyForTesting() {
        System.out.println("Done and tested: not ready");
    }

    @Override
    public void testing() {

    }

    @Override
    public void doneAndTesting() {
        if (this.backlogItem.getPerson().getType().equals("Developer")) {
            if(this.backlogItem.checkIfItemIsDone()){
                backlogItem.setBacklogState(backlogItem.getDoneState());
            } else{
                System.out.println("Item is not done");
                backlogItem.setBacklogState(backlogItem.getReadyForTestingState());
            }
        }
    }

    @Override
    public void doneState() {
        System.out.println("Done and tested: It is not done yet");
    }
}

package com.soa3.BacklogItemStates;

import com.soa3.BacklogItem;
import com.soa3.EmailObserver;

public class ReadyForTestingState implements IBacklogState {

    private BacklogItem backlogItem;

    public ReadyForTestingState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void toDo() {
        System.out.println("Ready for testing: Order is selected by developer and toDo is done");
    }

    @Override
    public void doing() {
        System.out.println("Ready for testing:It is done");
    }

    @Override
    public void readyForTesting() {
        //TODO notificatie moet naar alle testers dus moet nog rollen geimplementeerd worden
        new EmailObserver(this.backlogItem);
        this.backlogItem.notifyObservers("Ready for testing");


        if(this.backlogItem.isTestedIsSuccesfull()){
            backlogItem.setBacklogState(backlogItem.getTestingState());
        }else{
            System.out.println("gaat naar toDo");

            //TODO notification for scrummaster
            new EmailObserver(this.backlogItem);
            this.backlogItem.notifyObservers(this.backlogItem.getPerson().getName() + " Didn't finish his task");

            backlogItem.setBacklogState(backlogItem.getToDoState());
        }
    }

    @Override
    public void testing() {
        System.out.println("Ready for testing: Needs to be ready for testing");
    }

    @Override
    public void doneAndTesting() {
        System.out.println("Ready for testing: Needs to be ready for done and testing");
    }

    @Override
    public void doneState() {
        System.out.println("Ready for testing: not done");
    }
}

package com.soa3.BacklogItemStates;

import com.soa3.BacklogItem;

public class DoingState implements IBacklogState {

    private BacklogItem backlogItem;

    public DoingState(BacklogItem backlogItem) {
        this.backlogItem = backlogItem;
    }

    @Override
    public void toDo() {
        System.out.println("Doing state: Order is selected by developer");
    }

    @Override
    public void doing() {
        if(backlogItem.getPerson() != null){
            System.out.println(backlogItem.getPerson().getName() + " is working on the backlog item");
            System.out.println("Notificatie dat het done is");
            backlogItem.setBacklogState(backlogItem.getReadyForTestingState());
        }else{
            System.out.println("No developer is selected to this backlog item");
            backlogItem.setBacklogState(backlogItem.getToDoState());
        }
    }

    @Override
    public void readyForTesting() {
        System.out.println("Doing state: Select person first");
    }

    @Override
    public void testing() {
        System.out.println("Doing state: Test it first");
    }

    @Override
    public void doneAndTesting() {
        System.out.println("Doing state: It is not tested or done");
    }

    @Override
    public void doneState() {
        System.out.println("Doing state: It is not done");
    }
}

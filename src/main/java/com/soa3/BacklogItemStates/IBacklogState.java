package com.soa3.BacklogItemStates;

public interface IBacklogState {
    void toDo();
    void doing();
    void readyForTesting();
    void testing();
    void doneAndTesting();
    void doneState();
}
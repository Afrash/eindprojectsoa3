import com.soa3.*;
import com.soa3.BacklogItemStates.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BacklogTest {
    @Test
    void CreateProjectWithBacklogAndBacklogItems(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);

        assertEquals("projectnaam", project.getName());
        assertEquals(1, project.getId());
        assertEquals(3, project.getBacklog().getBacklogItems().size());
    }

    @Test
    void CreateActivitiesInBacklog(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);

        assertEquals("projectnaam", project.getName());
    }
    @Test
    void CreateBacklog(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);

        assertNotEquals(null, backlog);;
    }

    @Test
    void CreateBacklogActivities(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));

        assertEquals("create erd",   backlog.getBacklogItems().get(1).getActivities().get(0).getTitle());
    }

    @Test
    void DeveloperConnectedToSprintBacklogItem(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Developer","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));

        assertEquals("Developer",   backlog.getBacklogItems().get(1).getActivities().get(0).getPerson().getType());
    }

    @Test
    void MultipleDevelopersMultipleActivities(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Developer","Spoderman");
        Person p2 = new Person("Developer","Jan");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));
        backlog.getBacklogItems().get(1).addActivity(new Activity(p2,"create erd"));

        assertEquals("Developer",   backlog.getBacklogItems().get(1).getActivities().get(0).getPerson().getType());
        assertEquals("Developer",   backlog.getBacklogItems().get(1).getActivities().get(1).getPerson().getType());
    }


    @Test
    void SprintBackLogItemsToDoFase(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));
        //System.out.println(backlog.getBacklogItems().get(1).getBacklogState());
        assertEquals("ToDoState",backlog.getBacklogItems().get(1).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsDoingFase(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);
        backlog.getBacklogItems().get(1).addActivity(new Activity(p1,"create erd"));
        //System.out.println(backlog.getBacklogItems().get(1).getBacklogState());
        backlog.getBacklogItems().get(1).toDo();
        assertEquals("DoingState",backlog.getBacklogItems().get(1).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsReadyForTestingFase(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();

        assertEquals("ReadyForTestingState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsTestingFase(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        sprint.getBacklogItems().get(0).setTestedIsSuccesfull(true);

        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();
        sprint.getBacklogItems().get(0).getBacklogState().readyForTesting();


        assertEquals("TestingState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsTestingTestedNotSuccesfullBackToToDo(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(2).getActivities().get(0).setDone(true);
        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        backlog.getBacklogItems().get(2).setTestedIsSuccesfull(true);
        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();

        sprint.getBacklogItems().get(0).getBacklogState().readyForTesting();

        assertEquals("ToDoState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsTestingTestedAndDoneItemIsNotDone(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(2).getActivities().get(0).setDone(true);
        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        backlog.getBacklogItems().get(2).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(0).setTestedIsSuccesfull(true);
        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();

        sprint.getBacklogItems().get(0).getBacklogState().readyForTesting();
        sprint.getBacklogItems().get(0).getBacklogState().doneAndTesting();

        assertEquals("TestingState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsTestingTestedAndDoneItemIsDoneNow(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p5);
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p1);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"create erd"));
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(2).getActivities().get(0).setDone(true);
        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1,p1);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        backlog.getBacklogItems().get(2).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(0).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(0).setDone(true);
        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();

        sprint.getBacklogItems().get(0).getBacklogState().readyForTesting();
        sprint.getBacklogItems().get(0).getBacklogState().testing();
        sprint.getBacklogItems().get(0).getBacklogState().doneAndTesting();

        assertEquals("DoneAndTestedState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsTestingTestedAndDoneItemIsDoneState(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p5);
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p1);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"create erd"));
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(2).getActivities().get(0).setDone(true);
        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1,p1);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        backlog.getBacklogItems().get(2).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(0).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(0).setDone(true);
        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();

        sprint.getBacklogItems().get(0).getBacklogState().readyForTesting();
        sprint.getBacklogItems().get(0).getBacklogState().testing();
        sprint.getBacklogItems().get(0).getBacklogState().doneAndTesting();
        backlog.getBacklogItems().get(0).setPerson(p5);
        backlog.getBacklogItems().get(1).setPerson(p5);
        sprint.getBacklogItems().get(0).getBacklogState().doneState();

        assertEquals("DoneAndTestedState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }

    @Test
    void SprintBackLogItemsTestingTestedAndDoneItemIsDoneStateEveryState(){
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p5);
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p1);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"create erd"));
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(2).getActivities().get(0).setDone(true);
        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1,p1);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        backlog.getBacklogItems().get(2).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(0).setTestedIsSuccesfull(true);
        backlog.getBacklogItems().get(2).setDone(true);
        backlog.getBacklogItems().get(0).setDone(true);
        sprint.getBacklogItems().get(0).getBacklogState().toDo();
        sprint.getBacklogItems().get(0).getBacklogState().doing();

        sprint.getBacklogItems().get(0).getBacklogState().readyForTesting();
        sprint.getBacklogItems().get(0).getBacklogState().testing();
        sprint.getBacklogItems().get(0).getBacklogState().doneAndTesting();
        backlog.getBacklogItems().get(0).setPerson(p5);
        backlog.getBacklogItems().get(1).setPerson(p5);
        sprint.getBacklogItems().get(0).getBacklogState().doneState();

        ToDoState todostate = new ToDoState(backlog.getBacklogItems().get(0));
        todostate.doing();
        todostate.readyForTesting();
        todostate.testing();
        todostate.doneAndTesting();
        todostate.doneState();

        DoingState doingstate = new DoingState(backlog.getBacklogItems().get(0));
        doingstate.doing();
        doingstate.readyForTesting();
        doingstate.testing();
        doingstate.doneAndTesting();
        doingstate.doneState();

        ReadyForTestingState readyForTestingState = new ReadyForTestingState(backlog.getBacklogItems().get(0));
        readyForTestingState.doing();
        readyForTestingState.readyForTesting();
        readyForTestingState.testing();
        readyForTestingState.doneAndTesting();
        readyForTestingState.doneState();

        TestingState testingState = new TestingState(backlog.getBacklogItems().get(0));
        testingState.doing();
        testingState.readyForTesting();
        testingState.testing();
        testingState.doneAndTesting();
        testingState.doneState();

        DoneAndTestedState doneAndTestedState = new DoneAndTestedState(backlog.getBacklogItems().get(0));
        doneAndTestedState.doing();
        doneAndTestedState.readyForTesting();
        doneAndTestedState.testing();
        doneAndTestedState.doneAndTesting();
        doneAndTestedState.doneState();

        DoneState doneState = new DoneState(backlog.getBacklogItems().get(0));
        doneState.doing();
        doneState.readyForTesting();
        doneState.testing();
        doneState.doneAndTesting();
        doneState.doneState();


        assertEquals("DoneState",sprint.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
    }


}

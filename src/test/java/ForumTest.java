import com.soa3.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ForumTest {
    @Test
    void forum() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"review",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        Forum forum = new Forum();

        var forumSpy = Mockito.spy(forum);

        ForumThread forumThread = new ForumThread(backlog.getBacklogItems().get(0),p1);

        var forumThreadSpy = Mockito.spy(forumThread);

        forumSpy.addForumThread(forumThreadSpy);

        Reaction reaction = new Reaction("message",p2);

        var reactionSpy = Mockito.spy(reaction);

        forumThreadSpy.addReaction(reactionSpy);

        Reaction reply = new Reaction("reply",p3);

        var replySpy = Mockito.spy(reply);

        reactionSpy.addReply(replySpy);

        Mockito.verify(forumSpy, Mockito.times(1)).addForumThread(forumThreadSpy);

        Mockito.verify(forumThreadSpy, Mockito.times(1)).addReaction(reactionSpy);

        Mockito.verify(reactionSpy, Mockito.times(1)).addReply(replySpy);

        forumSpy.getForumThreads();

        Mockito.verify(forumSpy, Mockito.times(1)).getForumThreads();

        forumThreadSpy.getReactions();

        Mockito.verify(forumThreadSpy, Mockito.times(1)).getReactions();

        reactionSpy.getReplies();

        Mockito.verify(reactionSpy, Mockito.times(1)).getReplies();

    }
}

import com.soa3.*;
import com.soa3.ExportBehavior.SprintExportToPdfBehavior;
import com.soa3.ExportBehavior.SprintExportToPngBehavior;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ExportTest {

    @Test
    void TestedExportToPDFBehavior() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);

        var sprintSpy = Mockito.spy(sprint);

        var sprintExportToPdfBehavior = new SprintExportToPdfBehavior();

        sprintSpy.export(sprintExportToPdfBehavior);

        Mockito.verify(sprintSpy).export(sprintExportToPdfBehavior);

        Mockito.verify(sprintSpy, Mockito.times(1)).export(sprintExportToPdfBehavior);
    }

    @Test
    void TestedExportToPNGBehavior() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        persons.add(p1);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p1, p1);

        var sprintSpy = Mockito.spy(sprint);

        var sprintExportToPngBehavior = new SprintExportToPngBehavior();

        sprintSpy.export(sprintExportToPngBehavior);

        Mockito.verify(sprintSpy).export(sprintExportToPngBehavior);

        Mockito.verify(sprintSpy, Mockito.times(1)).export(sprintExportToPngBehavior);
    }


}

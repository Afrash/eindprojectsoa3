import com.soa3.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DevopsTest {
    @Test
    void runReleasePipeline() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        var sprintSpy = Mockito.spy(sprint);

        sprintSpy.runPipeline();

        Mockito.verify(sprintSpy, Mockito.times(1)).runPipeline();
    }

    @Test
    void runDevPipeline() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"review",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        var sprintSpy = Mockito.spy(sprint);

        sprintSpy.runPipeline();

        Mockito.verify(sprintSpy, Mockito.times(1)).runPipeline();
    }
    @Test
    void CreateDevPipeline() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"review",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        IDevops devops = new ReleaseDevPipeline(sprint);
        var devopsSpy = Mockito.spy(devops);
        sprint.runPipeline();

        devopsSpy.createSource();
        Mockito.verify(devopsSpy, Mockito.times(1)).createSource();
        devopsSpy.createPackages();
        Mockito.verify(devopsSpy, Mockito.times(1)).createPackages();
        devopsSpy.createBuild();
        Mockito.verify(devopsSpy, Mockito.times(1)).createBuild();
        devopsSpy.createTests();
        Mockito.verify(devopsSpy, Mockito.times(1)).createTests();
    }
    @Test
    void CreateReleasePipeline() throws Exception {

        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"review",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));
        IDevops devops = new ReleaseDevPipeline(sprint);
        sprint.runPipeline();
        var devopsSpy = Mockito.spy(devops);

        devopsSpy.createSource();
        Mockito.verify(devopsSpy, Mockito.times(1)).createSource();
        devopsSpy.createPackages();
        Mockito.verify(devopsSpy, Mockito.times(1)).createPackages();
        devopsSpy.createBuild();
        Mockito.verify(devopsSpy, Mockito.times(1)).createBuild();
        devopsSpy.createTests();
        Mockito.verify(devopsSpy, Mockito.times(1)).createTests();
        devopsSpy.createAnalysis();
        Mockito.verify(devopsSpy, Mockito.times(1)).createAnalysis();
        devopsSpy.createDeploy();
        Mockito.verify(devopsSpy, Mockito.times(1)).createDeploy();
        devopsSpy.createUtility();
        Mockito.verify(devopsSpy, Mockito.times(1)).createUtility();
    }
}

import com.soa3.*;
import com.soa3.SCM.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SCMTest {

    @Test
    void commit() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        ISCM scm = new SCM();

        var SCMSpy = Mockito.spy(scm);

        Commit commitCmd = new Commit(SCMSpy,"commit message");

        var commitSpy = Mockito.spy(commitCmd);

        CMD cmd = new CMD(commitSpy);

        var cmdSpy = Mockito.spy(cmd);

        cmdSpy.runCmd();

        Mockito.verify(cmdSpy).runCmd();
        Mockito.verify(cmdSpy, Mockito.times(1)).runCmd();
    }

    @Test
    void push() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        ISCM scm = new SCM();

        var SCMSpy = Mockito.spy(scm);

        Push pushCmd = new Push(SCMSpy);

        var pushSpy = Mockito.spy(pushCmd);

        CMD cmd = new CMD(pushSpy);

        var cmdSpy = Mockito.spy(cmd);

        cmdSpy.runCmd();

        Mockito.verify(cmdSpy).runCmd();
        Mockito.verify(cmdSpy, Mockito.times(1)).runCmd();
    }

    @Test
    void pull() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        ISCM scm = new SCM();

        var SCMSpy = Mockito.spy(scm);

        Pull pullCmd = new Pull(SCMSpy);

        var pullSpy = Mockito.spy(pullCmd);

        CMD cmd = new CMD(pullSpy);

        var cmdSpy = Mockito.spy(cmd);

        cmdSpy.runCmd();

        Mockito.verify(cmdSpy).runCmd();
        Mockito.verify(cmdSpy, Mockito.times(1)).runCmd();
    }

    @Test
    void createBranch() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1", LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        ISCM scm = new SCM();

        var SCMSpy = Mockito.spy(scm);

        Branch branchCmd = new Branch(SCMSpy,"branch name");

        var branchSpy = Mockito.spy(branchCmd);

        CMD cmd = new CMD(branchSpy);

        var cmdSpy = Mockito.spy(cmd);

        cmdSpy.runCmd();

        Mockito.verify(cmdSpy).runCmd();
        Mockito.verify(cmdSpy, Mockito.times(1)).runCmd();
    }
}

import com.soa3.*;
import com.soa3.ExportBehavior.SprintExportToPdfBehavior;
import com.soa3.ExportBehavior.SprintExportToPngBehavior;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class NotificationTest {

    @Test
    void NotificationTestEmail() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        var notificationObserver = new EmailObserver(sprint.getBacklogItems().get(0));
        var adapterSpy = Mockito.spy(notificationObserver.getObservable());

        //notificationObserver.notifyAll();
        adapterSpy.notifyObservers("Hello");

        //assertEquals("test",emailSpy.notifyAll());
        Mockito.verify(adapterSpy, Mockito.times(1)).notifyObservers("Hello");
    }

    @Test
    void NotificationTestSlack() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        Sprint sprint = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        sprint.addBacklogItem(backlog.getBacklogItems().get(0));

        var notificationObserver = new SlackObserver(sprint.getBacklogItems().get(0));
        var adapterSpy = Mockito.spy(notificationObserver.getObservable());

        //notificationObserver.notifyAll();
        adapterSpy.notifyObservers("Hello");

        //assertEquals("test",emailSpy.notifyAll());
        Mockito.verify(adapterSpy, Mockito.times(1)).notifyObservers("Hello");
    }

    @Test
    void NotificationTestEmailScrumMasterBackToToDoStateWithNotification() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));


        var mockTest = Mockito.spy(new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4));
        mockTest.addBacklogItem(backlog.getBacklogItems().get(0));

        mockTest.getBacklogItems().get(0).getBacklogState().toDo();
        mockTest.getBacklogItems().get(0).getBacklogState().doing();
        mockTest.getBacklogItems().get(0).getBacklogState().readyForTesting();

        var notificationObserver = new EmailObserver(mockTest.getBacklogItems().get(0));
        var adapterSpy = Mockito.spy(notificationObserver.getObservable());

        adapterSpy.notifyObservers("Matt Murdock Didn't finish his tasksend with Email");
        assertEquals("ToDoState",mockTest.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
        Mockito.verify(adapterSpy, Mockito.times(1)).notifyObservers("Matt Murdock Didn't finish his tasksend with Email");
    }

    @Test
    void NotificationTestEmailScrumMasterBackTestingWithNotification() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        var mockTest = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        mockTest.addBacklogItem(backlog.getBacklogItems().get(0));

        mockTest.getBacklogItems().get(0).setTestedIsSuccesfull(true);

        mockTest.getBacklogItems().get(0).getBacklogState().toDo();
        mockTest.getBacklogItems().get(0).getBacklogState().doing();
        mockTest.getBacklogItems().get(0).getBacklogState().readyForTesting();


        assertEquals("TestingState",mockTest.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());

        mockTest.addBacklogItem(backlog.getBacklogItems().get(0));


        var notificationObserver = new EmailObserver(mockTest.getBacklogItems().get(0));
        var adapterSpy = Mockito.spy(notificationObserver.getObservable());

        adapterSpy.notifyObservers("Testing state notification");
        assertEquals("TestingState",mockTest.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
        Mockito.verify(adapterSpy, Mockito.times(1)).notifyObservers("Testing state notification");
    }

    @Test
    void NotificationTestSMSScrumMasterBackTestingWithNotification() throws Exception {
        List<Person> persons = new ArrayList<>();
        List<Person> developers = new ArrayList<>();
        Person p1 = new Person("Tester","Spoderman");
        Person p2 = new Person("Developer", "Leonardo");
        Person p3 = new Person("Scrummaster", "Matt Murdock");
        Person p4 = new Person("Productowner", "PO");
        Person p5 = new Person("Developer", "Dev");
        developers.add(p2);
        developers.add(p5);
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Project project = new Project("projectnaam",1,persons);

        Backlog backlog = project.getBacklog();

        backlog.addBacklogItem(new BacklogItem("backend"));
        backlog.addBacklogItem(new BacklogItem("frontend"));
        backlog.addBacklogItem(new BacklogItem("database"));

        backlog.getBacklogItems().get(0).setPerson(p3);
        backlog.getBacklogItems().get(1).setPerson(p1);
        backlog.getBacklogItems().get(2).addActivity(new Activity(p2,"create erd"));
        backlog.getBacklogItems().get(2).addActivity(new Activity(p1,"setup database"));

        var mockTest = new Sprint("sprint 1",LocalDate.parse("2022-03-03"),LocalDate.parse("2022-05-05"),"release",developers,p3,p4);
        mockTest.addBacklogItem(backlog.getBacklogItems().get(0));

        mockTest.getBacklogItems().get(0).setTestedIsSuccesfull(true);

        mockTest.getBacklogItems().get(0).getBacklogState().toDo();
        mockTest.getBacklogItems().get(0).getBacklogState().doing();
        mockTest.getBacklogItems().get(0).getBacklogState().readyForTesting();

        assertEquals("TestingState",mockTest.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());

        mockTest.addBacklogItem(backlog.getBacklogItems().get(0));

        var notificationObserver = new SMSObserverAdapter(mockTest.getBacklogItems().get(0));
        var adapterSpy = Mockito.spy(notificationObserver.getObservable());

        adapterSpy.notifyObservers("Testing state notification");
        assertEquals("TestingState",mockTest.getBacklogItems().get(0).getBacklogState().getClass().getSimpleName());
        Mockito.verify(adapterSpy, Mockito.times(1)).notifyObservers("Testing state notification");
    }



}
